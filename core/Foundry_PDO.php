<?php
/**
 * Foundry
 *
 * LICENSE
 *
Copyright 2013 Virtuous Consulting Services

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

 * @Owner     Stephen Darley (http://www.darleys.org)
 * @Author     Stephen Darley (http://www.darleys.org)
 */
class Foundry_PDO {
    public static $DBCON;
    public $DBOBJ;
    private static $fDB;
    private static $types;
    private function __construct()  {
    }

    public static function FSingle()  {
        if (!isset(self::$fDB)) {
            $className = __CLASS__;
            self::$fDB = new $className;
        }
        self::$fDB->DBOBJ= self::connect();
        return self::$fDB;
    }

    public function connect() {
        self::$types = array(
            'VAR_STRING' => 'PDO::PARAM_STR',
            'BLOB' => 'PDO::PARAM_LOB',
            'DATETIME' => 'PDO::PARAM_STR',
            'TIMESTAMP' => 'PDO::PARAM_STR',
            'DATE' => 'PDO::PARAM_STR',
            'INT' => 'PDO::PARAM_INT',
            'TINY' => 'PDO::PARAM_INT',
            'LONG' => 'PDO::PARAM_INT'
        );

        if(self::$DBCON) {
            echo "!@CON";
            return self::$DBCON;
        }else {
            $dbconf = thisDB::$dbconfig;
            self::$DBCON =new PDO("mysql:host={$dbconf['host']};dbname={$dbconf['database']}", $dbconf['login'], $dbconf['password']);
            return self::$DBCON;
        }

    }
    public function FetchByID($table, $id)
    {
        try {
            $conn = self::$DBCON;
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $conn->prepare("SELECT * FROM {$table} WHERE id=:id");
            $stmt->execute(array(':id'=>$id));

            return $stmt->fetch(PDO::FETCH_ASSOC);

        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function FetchAll($table, $where, $page=0, $page_size)
    {
        $page = ($page * $page_size);
        try {
            $conn = self::$DBCON;
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $conn->prepare("SELECT * FROM {$table} WHERE deleted=0 {$where} LIMIT {$page}, {$page_size}");
            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function GetCount($table, $where)
    {
        try {
            $conn = self::$DBCON;
            if ($res = $conn->query("SELECT count(id) FROM {$table} WHERE deleted=0 {$where}"))
            {
                return $res->fetchColumn();
            }
        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function Update($table,$fields,$values,$where)
    {
        list($table_meta, $column_type) = self::GetColumnDataTypes($table);

        foreach($fields as $field)
            $pdo_fields[] = "{$field} = :{$field}";

        try {
            $conn = self::$DBCON;
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $conn->prepare("UPDATE {$table} SET ".implode(',',$pdo_fields)." WHERE id=:id");


            foreach($fields as $field) {
                $stmt->bindParam(':' . $field, self::SanitizeData($table_meta, $field, $values[$field]));
            }

            $stmt->execute();
            $stmt->debugDumpParams();
        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function Insert($table,$fields,$values)
    {
        list($table_meta, $column_type) = self::GetColumnDataTypes($table);

        foreach($fields as $field)
            $pdo_fields[] = ":{$field}";

        try {
            $conn = self::$DBCON;
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $conn->prepare("INSERT INTO  {$table} (".implode(',',$fields).") VALUES  (".implode(',',$pdo_fields).")");

            foreach($fields as $field) {
                $stmt->bindParam(':' . $field, self::SanitizeData($table_meta, $field, $values[$field]));
            }

            $stmt->execute();
            $stmt->debugDumpParams();
            return $conn->lastInsertId();

        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function GetColumnDataTypes($table)
    {
        $conn = self::$DBCON;

        $stmt = $conn->prepare("SELECT * from {$table} LIMIT 1");
        $stmt->execute();

        //GET COLUMN DATA TYPE
        foreach(range(0, $stmt->columnCount() - 1) as $column_index)
        {
            $meta = $stmt->getColumnMeta($column_index);
            $table_meta[$meta['name']] = $meta;
            $column_type[$meta['name']] = self::$types[$meta['native_type']];
        }
        return array($table_meta,$column_type);
    }

    public function RemoveByID($table, $id)
    {
        try {
            $conn = self::$DBCON;
            $stmt = $conn->prepare("DELETE FROM {$table} WHERE id=:id");
            $stmt->execute(array(':id'=>$id));
        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function SanitizeData($table_meta, $field, $data)
    {
        //SANITIZE DATE
        if ($table_meta[$field]['native_type'] == 'DATE' && $data !=''){
            $data = date("Y-m-d", strtotime($data));
            return $data;
        }else{
            return $data;
        }
    }




}


?>